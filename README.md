## missi-user 12
12 SKQ1.211006.001 V13.0.1.0.SKKMIXM release-keys
- Manufacturer: xiaomi
- Platform: lahaina
- Codename: haydn
- Brand: Xiaomi
- Flavor: missi-user
- Release Version: 12
12
- Id: SKQ1.211006.001
- Incremental: V13.0.1.0.SKKMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: 560
- Fingerprint: Xiaomi/haydn/haydn:12/RKQ1.211001.001/V13.0.1.0.SKKMIXM:user/release-keys
- OTA version: 
- Branch: missi-user-12
12-SKQ1.211006.001-V13.0.1.0.SKKMIXM-release-keys
- Repo: xiaomi/haydn
